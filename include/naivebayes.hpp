#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <string>

std::vector<std::string> tokenize(std::string s);

float logPriorProb(int categ_freq, int num_docs);

float logTermCategProb(float categ_term_count, float categ_total_terms);

void train(std::string filename, 
           int &num_training_docs,
           std::vector<std::string> &term_vector, 
           std::vector<std::string> &class_vector, 
           std::map<std::string, std::map<std::string, int>> &term_class_freq_map, 
           std::map<std::string, std::vector<int>> &class_info_map);

float* learn(std::vector<std::string> &term_vector, 
             std::vector<std::string> &class_vector, 
             std::map<std::string, std::map<std::string, int>> &term_class_freq_map, 
             std::map<std::string, std::vector<int>> &class_info_map);

std::vector<int> test(std::string filename, 
                      int &num_training_docs, 
                      float *prob_matrix, 
                      std::vector<std::string> &term_vector, 
                      std::vector<std::string> &class_vector, 
                      std::map<std::string, std::vector<int>> &class_info_map);