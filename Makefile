# cuda flags
CC = aarch64-linux-gnu-g++
NVCC = /usr/local/cuda-10.0/bin/nvcc -ccbin $(CC)
NVCC_LIB = /usr/local/cuda-10.0/targets/aarch64-linux/lib
INCLUDES = /usr/local/cuda-10.0/samples/common/inc
CUDART_LIB = /usr/local/cuda-10.0/targets/aarch64-linux/lib/libcudart.so

# cpu flags
CXX = g++
CXXFLAGS = -std=c++0x -fopenmp
CFLAGS = -Wall -Wextra

nb-gpu.out: nb-gpu.o
	$(CC) $(CXXFLAGS) -o nb-gpu.out nb-gpu.o -L$(NVCC_LIB) -lcudart

nb-gpu.o: ./src/nb-gpu.cu
	$(NVCC) -I. -I$(INCLUDES) -c ./src/nb-gpu.cu

nb-cpu.out: nb-cpu.o naivebayes.o
	$(CXX) -o nb-cpu.out nb-cpu.o naivebayes.o $(CXXFLAGS)

nb-cpu.o: ./src/nb-cpu.cpp
	$(CXX) -c ./src/nb-cpu.cpp $(CXXFLAGS)

naivebayes.o: ./src/naivebayes.cpp ./include/naivebayes.hpp
	$(CXX) -c ./src/naivebayes.cpp $(CXXFLAGS)

clean:
	rm -rf *.exe *.o *.stackdump *~ *.out *.tmp
