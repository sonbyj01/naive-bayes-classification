#include "../include/naivebayes.hpp"
#include <fstream>
#include <sstream>
#include <iterator>
#include <cmath>
#include <algorithm>
#include "omp.h"

// https://stackoverflow.com/questions/485230/c-tokenizing-using-iterators-in-an-eof-cycle
std::vector<std::string> tokenize(std::string s) {
    std::istringstream iss(s);
    std::vector<std::string> tokens((std::istream_iterator<std::string>(iss)), std::istream_iterator<std::string>());

    return tokens;
}

float logPriorProb(int categ_freq, int num_docs) {
    return log(categ_freq / num_docs);
}

float logTermCategProb(float categ_term_count, float categ_total_terms) {
    return log((categ_term_count + 1) / (categ_total_terms));
}

void train(std::string filename, int &num_training_docs, std::vector<std::string> &term_vector, std::vector<std::string> &class_vector, std::map<std::string, std::map<std::string, int>> &term_class_freq_map, std::map<std::string, std::vector<int>> &class_info_map) {
    std::ifstream file(filename);
    std::string line;

    while(std::getline(file, line)) {
        std::vector<std::string> doc_split = tokenize(line);
        std::string doc_class = doc_split[0];

        std::vector<std::string>::iterator class_it = std::find(class_vector.begin(), class_vector.end(), doc_class);

        if(class_it == class_vector.end()) {
            class_vector.push_back(doc_class);

            std::vector<int> class_info_vector;
            class_info_vector.push_back(0);
            class_info_vector.push_back(0);

            class_info_map[doc_class] = class_info_vector;
        }

        class_info_map[doc_class][0] += 1;

        for(int i = 1; i < doc_split.size(); i++) {
            std::string term = doc_split[i];

            std::vector<std::string>::iterator term_it = std::find(term_vector.begin(), term_vector.end(), term);

            if(term_it == term_vector.end()) {
                term_vector.push_back(term);
            }

            if(term_class_freq_map.count(term) == 0) {
                term_class_freq_map[term][doc_class] = 1;
            } else {
                term_class_freq_map[term][doc_class] += 1;
            }

            class_info_map[doc_class][1] += 1;
        }
        num_training_docs++;
    }
}

float* learn(std::vector<std::string> &term_vector, std::vector<std::string> &class_vector, std::map<std::string, std::map<std::string, int>> &term_class_freq_map, std::map<std::string, std::vector<int>> &class_info_map) {
    float *prob_matrix = (float *)calloc((term_vector.size() * class_vector.size()), sizeof(float));

    if(prob_matrix == NULL) {
        std::cerr << "error allocating prob_matrix" << std::endl;
        exit(-1);
    }

    #pragma omp parallel for num_threads(4)
    for(int t_index = 0; t_index < term_vector.size(); t_index++) {
        std::string curr_term = term_vector[t_index];

        for(int c_index = 0; c_index < class_vector.size(); c_index++) {
            std::string curr_class = class_vector[c_index];
            float curr_freq = (float) term_class_freq_map[curr_term][curr_class];
            float curr_total_terms = (float) class_info_map[curr_class][1];

            float log_prob = logTermCategProb(curr_freq, curr_total_terms);

            *(prob_matrix + (t_index * class_vector.size()) + c_index) = log_prob;
        }
    }
    
    return prob_matrix;
}

std::vector<int> test(std::string filename, int &num_training_docs, float *prob_matrix, std::vector<std::string> &term_vector, std::vector<std::string> &class_vector, std::map<std::string, std::vector<int>> &class_info_map) {
    std::vector<int> results;

    std::ifstream file(filename);
    std::string line;

    while(std::getline(file, line)) {
        std::vector<std::string> doc_terms = tokenize(line);
        std::map<std::string, int> term_count;

        for(int i = 1; i < doc_terms.size(); i++) {
            std::string term = doc_terms[i];

            if(term_count.count(term) == 0) {
                term_count[term] = 1;
            } else {
                term_count[term] += 1;
            }
        }

        std::vector<float> all_class_log_probs;

        for(int i = 0; i < class_vector.size(); i++) {
            float categ_freq = (float) class_info_map[class_vector[i]][0];
            float log_prior = logPriorProb(categ_freq, (float) num_training_docs);
            float log_class_prob = 0;

            for(std::map<std::string, int>::iterator it = term_count.begin(); it != term_count.end(); ++it) {
                std::string term = it->first;
                int count = it->second;

                std::vector<std::string>::iterator term_it = std::find(term_vector.begin(), term_vector.end(), term);
                if(term_it == term_vector.end()) {
                    log_class_prob += 0;
                } else {
                    int t_index = term_it - term_vector.begin();
                    int c_index = i;
                    float log_prob = *(prob_matrix + (t_index * class_vector.size()) + c_index); 

                    log_class_prob += (log_prob * count);
                }
            }
            all_class_log_probs.push_back(log_prior + log_class_prob);
        }
        int max_index = std::distance(all_class_log_probs.begin(), std::max_element(all_class_log_probs.begin(), all_class_log_probs.end()));
        results.push_back(max_index);
    }
    return results;
}