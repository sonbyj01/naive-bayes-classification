#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <chrono>
#include <cuda_profiler_api.h>
#include <cuda_runtime.h>

#define timeNow() std::chrono::high_resolution_clock::now()
#define duration_ms(start, stop) std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count()
#define duration_us(start, stop) std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()

typedef std::chrono::high_resolution_clock::time_point TimeStamp;

__global__ void calcFreq(int *term_index_array, int *doc_term_array, int *doc_class, float *term_class_matrix, int num_terms, int doc_term_len, int classes) {
    unsigned int i = blockIdx.x * gridDim.y * gridDim.z *
                     blockDim.x + blockIdx.y * gridDim.z *
                     blockDim.x + blockIdx.z * blockDim.x + threadIdx.x;

    int start = term_index_array[i];
    int end = term_index_array[i];

    if(i < num_terms - 1) {
        end = term_index_array[i+1];
    } else if(i == num_terms - 1) {
        end = doc_term_len - 1;
    } else {
        return ;
    }

    for(int x = start; x < end; x++) {
        term_class_matrix[classes * i + doc_class[doc_term_array[x]]] += 1.0;
    }
}

__global__ void calcTotalTermsPerClass(float *term_class_matrix, int *terms_per_class, int num_terms, int classes) {
    unsigned int i = blockIdx.x * gridDim.y * gridDim.z * 
                     blockDim.x + blockIdx.y * gridDim.z *
                     blockDim.x + blockIdx.z * blockDim.x + threadIdx.x;
    
    if(i < classes) {
        int sum = 0;
        
        for(int x = 0; x < num_terms; x++) {
            sum += (int) term_class_matrix[classes * x + i];
        }

        terms_per_class[i] = sum;
    }
}

__global__ void learn(float *term_class_matrix, int num_docs, int classes, int *terms_per_class, int num_terms) {
    unsigned int i = blockIdx.x * gridDim.y * gridDim.z *
                     blockDim.x + blockIdx.y * gridDim.z *
                     blockDim.x + blockIdx.z * blockDim.x + threadIdx.x;

    float k = 1.0;

    if(i < num_terms) {
        for(int x = 0; x < classes; x++) {
            term_class_matrix[classes * i + x] = logf((term_class_matrix[classes * i + x] + k) / (terms_per_class[x] + k * num_terms));
        }
    }
}

__global__ void test(float *term_class_matrix, float *doc_prob, int *doc_index, int *terms_in_doc, int classes, int num_docs, int total_len_terms, int *predictions, float *prior) {
    unsigned int i = blockIdx.x * gridDim.y * gridDim.z *
                     blockDim.x + blockIdx.y * gridDim.z *
                     blockDim.x + blockIdx.z * blockDim.x + threadIdx.x;
    
    int start_term = doc_index[i];
    int end_term = doc_index[i];

    if(i < num_docs - 1) {
        end_term = doc_index[i+1];
    } else if(i == num_docs - 1) {
        end_term = total_len_terms - 1;
    } else {
        return ;
    }

    for(int x = start_term; x < end_term; x++) {
        for(int y = 0; y < classes; y++) {
            doc_prob[classes * i + y] += term_class_matrix[classes * terms_in_doc[x] + y];
        }
    }

    int max_index = 0;
    float max = logf(0.0);

    for(int y = 0; y < classes; y++) {
        if(doc_prob[classes * i + y] + logf(prior[y]) > max) {
            max_index = y;
            max = doc_prob[classes * i + y] + logf(prior[y]);
        }
    }
    predictions[i] = max_index;
}

void errorCheck(cudaError_t err) {
    if(err) {
        fprintf(stderr, "CUDA error: %d\n", err);
        exit(err);
    }
}

static cudaError_t numBlocksThreads(unsigned int N, dim3 *numBlocks, dim3 *threadsPerBlock) {
    unsigned int BLOCKSIZE = 128;
    int Nx, Ny, Nz;
    int device;
    cudaError_t err;

    if(N < BLOCKSIZE) {
        numBlocks->x = 1;
        numBlocks->y = 1;
        numBlocks->z = 1;

        threadsPerBlock->x = N;
        threadsPerBlock->y = 1;
        threadsPerBlock->z = 1;
        
        return cudaSuccess;
    }

    threadsPerBlock->x = BLOCKSIZE;
    threadsPerBlock->y = 1;
    threadsPerBlock->z = 1;

    err = cudaGetDevice(&device);
    if(err) {
        return err;
    }

    err = cudaDeviceGetAttribute(&Nx, cudaDevAttrMaxBlockDimX, device);
    if(err) {
        return err;
    }

    err = cudaDeviceGetAttribute(&Ny, cudaDevAttrMaxBlockDimY, device);
    if(err) {
        return err;
    }

    err = cudaDeviceGetAttribute(&Nz, cudaDevAttrMaxBlockDimZ, device);
    if(err) {
        return err;
    }
    
    unsigned int n = (N - 1) / BLOCKSIZE + 1;
    unsigned int x = (n - 1) / (Ny * Nz) + 1;
    unsigned int y = (n - 1) / (x * Nz) + 1;
    unsigned int z = (n - 1) / (x * y) + 1;
    if(x > Nx || y > Ny || z > Nz) {
        return cudaErrorInvalidConfiguration;
    }

    numBlocks->x = x;
    numBlocks->y = y;
    numBlocks->z = z;

    return cudaSuccess;
}

int *vectorToArrayInteger(std::vector<int> v) {
    int *arr = (int *)malloc(v.size() * sizeof(int));

    if(arr == NULL) {
        std::cerr << "Error converting vector to array" << std::endl;
        exit(-1);
    }

    std::copy(v.begin(), v.end(), arr);

    return arr;
}

float *vectorToArrayFloat(std::vector<float> v) {
    float *arr = (float *)malloc(v.size() * sizeof(float));

    if(arr == NULL) {
        std::cerr << "Error converting vector to array" << std::endl;
        exit(-1);
    }

    std::copy(v.begin(), v.end(), arr);

    return arr;
}

int main(int argc, char **argv) {
    if(argc != 4) {
        std::cerr << "Usage: " << argv[0] << " [train_file] [test_file] [output_file]" << std::endl;
        exit(-1);
    }

    std::vector<std::string> term_vector;

    std::map<std::string, std::vector<int>> term_doc_map;

    std::vector<int> term_index_vector;

    std::vector<int> doc_term_vector;

    std::vector<int> doc_class;

    std::vector<std::string> classes_vector;

    std::vector<float> prior_vector;

    std::ifstream file(argv[1]);
    std::string line;
    int lineno = 0;

    while(std::getline(file, line)) {
        std::istringstream iss(line);
        std::vector<std::string> doc_split((std::istream_iterator<std::string>(iss)), std::istream_iterator<std::string>());

        std::vector<std::string>::iterator class_it = std::find(classes_vector.begin(), classes_vector.end(), doc_split[0]);
        if(class_it == classes_vector.end()) {
            classes_vector.push_back(doc_split[0]);
            prior_vector.push_back(0.0);
        }

        int class_index = std::find(classes_vector.begin(), classes_vector.end(), doc_split[0]) - classes_vector.begin();
        doc_class.push_back(class_index);
        prior_vector[class_index] += 1.0;

        for(int i = 1; i < doc_split.size(); i++) {
            std::string term = doc_split[i];

            std::vector<std::string>::iterator term_it = std::find(term_vector.begin(), term_vector.end(), term);
            if(term_it == term_vector.end()) {
                term_vector.push_back(term);
            }

            std::vector<int> doc_list = term_doc_map[term];
            std::vector<int>::iterator doc_it = std::find(doc_list.begin(), doc_list.end(), lineno);
            if(doc_it == doc_list.end()) {
                doc_list.push_back(lineno);
                term_doc_map[term] = doc_list;
            }
        }
        lineno++;
    }

    for(int i = 0; i < classes_vector.size(); i++) {
        prior_vector[i] /= doc_class.size();
    }

    for(int index = 0; index < term_vector.size(); index++) {
        std::string t = term_vector[index];

        std::vector<int> d = term_doc_map[t];

        term_index_vector.push_back(doc_term_vector.size());
        
        doc_term_vector.insert(doc_term_vector.end(), d.begin(), d.end());
    }

    /**/
    std::ifstream test_file(argv[2]);
    std::vector<int> test_doc_index_vector;
    std::vector<int> test_term_doc_vector;

    while(std::getline(test_file, line)) {
        std::istringstream iss(line);
        std::vector<std::string> doc_split((std::istream_iterator<std::string>(iss)), std::istream_iterator<std::string>());

        std::vector<int> test_doc_terms;
        for(int i = 0; i < doc_split.size(); i++) {
            std::string term = doc_split[i];
            std::vector<std::string>::iterator term_it = std::find(term_vector.begin(), term_vector.end(), term);

            if(term_it != term_vector.end()) {
                test_doc_terms.push_back(term_it - term_vector.begin());
            } else {
                continue;
            }
        }

        test_doc_index_vector.push_back(test_term_doc_vector.size());
        test_term_doc_vector.insert(test_term_doc_vector.end(), test_doc_terms.begin(), test_doc_terms.end());
    }

    int *term_index_array = vectorToArrayInteger(term_index_vector);
    int *doc_term_array = vectorToArrayInteger(doc_term_vector);
    int *doc_class_array = vectorToArrayInteger(doc_class);

    float *term_class_matrix = (float *)calloc((term_vector.size()) * (classes_vector.size()), sizeof(float));

    int *total_terms_class_array = (int *)calloc(classes_vector.size(), sizeof(int));

    size_t nSpatial;
    size_t mSpatial;
    dim3 spatialThreadsPerBlock, spatialBlocks;

    float *d_term_class;
    int *d_term_index;
    int *d_doc_term;
    int *d_doc_class;
    int *d_total_terms_class;

    int *test_doc_index_array = vectorToArrayInteger(test_doc_index_vector);
    int *test_term_doc_array = vectorToArrayInteger(test_term_doc_vector);
    float *prior_array = vectorToArrayFloat(prior_vector);

    int *predictions = (int *)calloc(test_doc_index_vector.size(), sizeof(int));

    float *test_doc_prob = (float *)calloc((test_doc_index_vector.size()) * (classes_vector.size()), sizeof(float));
    float *d_test_doc_prob;

    int *d_test_doc_index;
    int *d_test_term_doc;
    int *d_predictions;
    float *d_prior;

    cudaDeviceReset();
    cudaProfilerStart();

    /* document term vector */
    nSpatial = doc_term_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));
    mSpatial = spatialBlocks.x * spatialBlocks.y * spatialBlocks.z * spatialThreadsPerBlock.x * sizeof(int);
    errorCheck(cudaMalloc(&d_doc_term, mSpatial));
    errorCheck(cudaMemcpy(d_doc_term, doc_term_array, nSpatial * sizeof(int), cudaMemcpyHostToDevice));

    /* training document classes */
    nSpatial = doc_class.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));
    mSpatial = spatialBlocks.x * spatialBlocks.y * spatialBlocks.z * spatialThreadsPerBlock.x * sizeof(int);
    errorCheck(cudaMalloc(&d_doc_class, mSpatial));
    errorCheck(cudaMemcpy(d_doc_class, doc_class_array, nSpatial * sizeof(int), cudaMemcpyHostToDevice));

    /* probability matrix */
    nSpatial = term_vector.size() * classes_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));
    mSpatial = spatialBlocks.x * spatialBlocks.y * spatialBlocks.z * spatialThreadsPerBlock.x * sizeof(float);
    errorCheck(cudaMalloc(&d_term_class, mSpatial));
    errorCheck(cudaMemcpy(d_term_class, term_class_matrix, nSpatial * sizeof(float), cudaMemcpyHostToDevice));

    /* allocation of arrays based on class size */
    nSpatial = classes_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));

    /* total terms valid in each class */
    mSpatial = spatialBlocks.x * spatialBlocks.y * spatialBlocks.z * spatialThreadsPerBlock.x * sizeof(int);
    errorCheck(cudaMalloc(&d_total_terms_class, mSpatial));
    errorCheck(cudaMemcpy(d_total_terms_class, total_terms_class_array, nSpatial * sizeof(int), cudaMemcpyHostToDevice));

    /* prior probability for each class */
    mSpatial = spatialBlocks.x * spatialBlocks.y * spatialBlocks.z * spatialThreadsPerBlock.x * sizeof(float);
    errorCheck(cudaMalloc(&d_prior, mSpatial));
    errorCheck(cudaMemcpy(d_prior, prior_array, nSpatial * sizeof(float), cudaMemcpyHostToDevice));

    /* test documents' probability for each class */
    nSpatial = test_doc_index_vector.size() * classes_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));
    mSpatial = spatialBlocks.x * spatialBlocks.y * spatialBlocks.z * spatialThreadsPerBlock.x * sizeof(float);
    errorCheck(cudaMalloc(&d_test_doc_prob, mSpatial));
    errorCheck(cudaMemcpy(d_test_doc_prob, test_doc_prob, nSpatial * sizeof(float), cudaMemcpyHostToDevice));

    /* allocation based on number of test documents */
    nSpatial = test_doc_index_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));
    mSpatial = spatialBlocks.x * spatialBlocks.y * spatialBlocks.z * spatialThreadsPerBlock.x * sizeof(int);

    /* array where each element represents where in the array the document's terms start in the test_term_doc_array */
    errorCheck(cudaMalloc(&d_test_doc_index, mSpatial));
    errorCheck(cudaMemcpy(d_test_doc_index, test_doc_index_array, nSpatial * sizeof(int), cudaMemcpyHostToDevice));

    /* holds the prediction for each test document */
    errorCheck(cudaMalloc(&d_predictions, mSpatial));
    errorCheck(cudaMemcpy(d_predictions, predictions, nSpatial * sizeof(int), cudaMemcpyHostToDevice));

    nSpatial = test_term_doc_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));
    mSpatial = spatialBlocks.x * spatialBlocks.y * spatialBlocks.z * spatialThreadsPerBlock.x * sizeof(int);
    errorCheck(cudaMalloc(&d_test_term_doc, mSpatial));
    errorCheck(cudaMemcpy(d_test_term_doc, test_term_doc_array, nSpatial * sizeof(int), cudaMemcpyHostToDevice));

    nSpatial = term_index_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));
    mSpatial = spatialBlocks.x * spatialBlocks.y * spatialBlocks.z * spatialThreadsPerBlock.x * sizeof(int);
    errorCheck(cudaMalloc(&d_term_index, mSpatial));
    errorCheck(cudaMemcpy(d_term_index, term_index_array, nSpatial * sizeof(int), cudaMemcpyHostToDevice));

    /* --- TRAINING --- */
    std::cerr << "training..." << std::endl;
    TimeStamp train_start = timeNow();
    
    calcFreq<<<spatialBlocks, spatialThreadsPerBlock>>>(d_term_index, d_doc_term, d_doc_class, d_term_class, term_vector.size(), doc_term_vector.size(), classes_vector.size());
    nSpatial = classes_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));
    calcTotalTermsPerClass<<<spatialBlocks, spatialThreadsPerBlock>>>(d_term_class, d_total_terms_class, term_vector.size(), classes_vector.size());
    cudaDeviceSynchronize();

    TimeStamp train_stop = timeNow();
    std::cerr << "Done (" << duration_ms(train_start, train_stop) << " ms)" << std::endl;

    nSpatial = term_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));

    /* --- LEARNING --- */
    std::cerr << "learning..." << std::endl;
    TimeStamp learn_start = timeNow();

    learn<<<spatialBlocks, spatialThreadsPerBlock>>>(d_term_class, doc_class.size(), classes_vector.size(), d_total_terms_class, term_vector.size());
    cudaDeviceSynchronize();

    TimeStamp learn_stop = timeNow();
    std::cerr << "Done (" << duration_us(learn_start, learn_stop) << " us)" << std::endl;

    nSpatial = test_doc_index_vector.size();
    errorCheck(numBlocksThreads(nSpatial, &spatialBlocks, &spatialThreadsPerBlock));

    /* --- TESTING --- */
    std::cerr << "testing..." << std::endl;
    TimeStamp test_start = timeNow();

    test<<<spatialBlocks, spatialThreadsPerBlock>>>(d_term_class, d_test_doc_prob, d_test_doc_index, d_test_term_doc, classes_vector.size(), test_doc_index_vector.size(), test_term_doc_vector.size(), d_predictions, d_prior);
    cudaDeviceSynchronize();

    TimeStamp test_stop = timeNow();
    std::cerr << "Done (" << duration_ms(test_start, test_stop) << " ms)" << std::endl;

    /* --- OUTPUT --- */
    errorCheck(cudaMemcpy(predictions, d_predictions, nSpatial * sizeof(int), cudaMemcpyDeviceToHost));
    std::ofstream results(argv[3]);
    if(results.is_open()) {
        for(int i = 0; i < test_doc_index_vector.size(); i++) {
            results << classes_vector[predictions[i]] << '\n';
        }
    }
    cudaProfilerStop();
    cudaDeviceReset();
}