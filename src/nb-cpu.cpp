#include "../include/naivebayes.hpp"
#include <chrono>
#include <fstream>

#define timeNow() std::chrono::high_resolution_clock::now()
#define duration_s(start, stop) std::chrono::duration_cast<std::chrono::seconds>(stop - start).count()
#define duration_ms(start, stop) std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count()

typedef std::chrono::high_resolution_clock::time_point TimeStamp;

int main(int argc, char **argv) {
    if(argc != 4) {
        std::cerr << "Usage: " << argv[0] << " [train_file] [test_file] [output_file]" << std::endl;
        exit(-1);
    }

    std::string train_filename = argv[1];
    std::string test_filename = argv[2];
    std::string output_filename = argv[3];

    int num_training_docs = 0;

    std::vector<std::string> term_vector;
    std::vector<std::string> classes_vector;
    std::map<std::string, std::map<std::string, int>> term_class_freq_map;
    std::map<std::string, std::vector<int>> class_info_map;

    std::cerr << "training..." << std::endl;
    TimeStamp train_start = timeNow();
    train(train_filename, num_training_docs, term_vector, classes_vector, term_class_freq_map, class_info_map);
    TimeStamp train_stop = timeNow();
    std::cerr << "done (" << duration_s(train_start, train_stop) << " s)" << std::endl;

    std::cerr << "learning..." << std::endl;
    TimeStamp learn_start = timeNow();
    float *prob_matrix = learn(term_vector, classes_vector, term_class_freq_map, class_info_map);
    TimeStamp learn_stop = timeNow();
    std::cerr << "done (" << duration_ms(learn_start, learn_stop) << " ms)" << std::endl;

    std::cerr << "testing..." << std::endl;
    TimeStamp test_start = timeNow();
    std::vector<int> results = test(test_filename, num_training_docs, prob_matrix, term_vector, classes_vector, class_info_map);
    TimeStamp test_stop = timeNow();
    std::cerr << "done (" << duration_s(test_start, test_stop) << " s)" << std::endl;

    std::ofstream outfile(output_filename);
    for(int i = 0; i < results.size(); i++) {
        std::string test_doc_class = classes_vector[results[i]];
        outfile << test_doc_class << std::endl;
    }
    outfile.close();
    std::cout << "Predictions can be found in " << output_filename << std::endl;
}