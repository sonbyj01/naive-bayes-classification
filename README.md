# Naïve Bayes Classification using C++ CUDA
ECE 453 - Advance Computer Architecture Final Project

## Datasets
[20ng](http://qwone.com/~jason/20Newsgroups/)


# Usage
```bash
// CPU Implementation
$ make nb-cpu.out
$ ./nb-cpu.out [train_file] [test_file] [output_file]

// GPU Implementation
$ make nb-gpu.out
$ ./nb-gpu.out [train_file] [test_file] [output_file]

// Test for F1-Score or Accuracy
// (must run either the GPU or CPU implementation first)
$ ./metrics.py [f1 | accuracy] [classifier_output] [true_output]

// Clean up executables, etc. 
$ make clean
```

# Sample Run
```bash
$ make nb-cpu.out
$ ./nb-cpu.out docs/train/r8.txt docs/test/r8.txt r8-output-cpu.tmp
$ ./metrics.py f1 r8-output-cpu.tmp docs/labels/r8.txt

$ make nb-gpu.out
$ ./nb-gpu.out docs/train/r8.txt docs/test/r8.txt r8-output-gpu.tmp
$ ./metrics.py f1 r8-output-gpu.tmp docs/labels/r8.txt

$ make clean
```